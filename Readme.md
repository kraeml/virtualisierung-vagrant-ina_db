# Virtualisierung

Wikipedia: https://de.wikipedia.org/wiki/Virtualisierung_(Informatik)

## Virtuelle Maschinen

Wikipedia: 

* [Virtuelle Maschine](https://de.wikipedia.org/wiki/Virtuelle_Maschine)

    * [Systembasierte virtuelle Maschinen](https://de.wikipedia.org/wiki/Virtuelle_Maschine#Systembasierte_virtuelle_Maschinen)
    * [Vor- und Nachteile](https://de.wikipedia.org/wiki/Virtuelle_Maschine#Vor-_und_Nachteile_des_Einsatzes_systembasierter_virtueller_Maschinen)

## Was wird benötigt?

* git
* VirtualBox
* vagrant
* packer (optional)

Download Links folgen später.

## Git

### Linux

Das Kommando `git` kann unter Ubuntu mit `sudo apt install git` installiert werden.

### Windows

Für Windows wird die [Git-Bash](https://gitforwindows.org/) benötigt.

> Git BASH  
>
> Git for Windows provides a BASH emulation used to run Git from the command line. *NIX users should feel right at home, as the BASH emulation behaves just like the "git" command in LINUX and UNIX environments.

Im Link Download findet man die aktuelle Version der git-bash für Windows.

## Virtualbox

Wikipedia: 

* [VirtualBox](https://de.wikipedia.org/wiki/VirtualBox)

Downloads:
* https://www.virtualbox.org/wiki/Downloads
* https://download.virtualbox.org/virtualbox/6.1.14/Oracle_VM_VirtualBox_Extension_Pack-6.1.14.vbox-extpack

![VirtualBox 6.1.14](./pictures/Virtualbox6.1.14.png)

__Arbeitsauftrag:__

1. Installation von Virtualbox mit Extension Pack

In der Bash (git-bash) sollte kann die Installation mit folgendem Kommando überprüft werden.
__Hinweis:__ Die Versionsnummer kann sich (Juli 2021) geändert haben.


```bash
VBoxManage --version
```

    6.1.22r144080


## Vagrant

Wikipedia:
* [Vagrant Software](https://de.wikipedia.org/wiki/Vagrant_(Software))

Download: https://www.vagrantup.com/downloads

__Arbeitsauftrag:__

1. Installation von Vagrant     
   * https://learn.hashicorp.com/tutorials/vagrant/getting-started-install
   
In der Bash (git-bash) sollte kann die Installation mit folgendem Kommando überprüft werden.
__Hinweis:__ Die Versionsnummer kann sich (Juli 2021) geändert haben.


```bash
vagrant --version
```

    Vagrant 2.2.17


__Hausaufgabe:__

Wegen Bandbreite von (W)LAN, bitte zu Hause ausführen:

```bash
vagrant box add rdf/ubuntu2004-de-devops
```

Tutorials:

* https://t3n.de/news/vagrant-gelingt-einstieg-883308/

* https://learn.hashicorp.com/collections/vagrant/getting-started

* https://www.taniarascia.com/what-are-vagrant-and-virtualbox-and-how-do-i-use-them/

* https://serversforhackers.com/s/vagrant

* https://riptutorial.com/de/vagrant

* https://d-mueller.de/blog/vagrant-tutorial/

## Mit Git INA und DB einrichten

Arbeitsauftrag:

1. Projektverzeichnis mit git klonen.


```bash
git clone https://gitlab.com/kraeml/virtualisierung-vagrant-ina_db.git
```

    Klone nach 'virtualisierung-vagrant-ina_db' ...
    remote: Enumerating objects: 20, done.[K
    remote: Counting objects: 100% (20/20), done.[K
    remote: Compressing objects: 100% (19/19), done.[K
    remote: Total 20 (delta 6), reused 0 (delta 0), pack-reused 0[K
    Entpacke Objekte: 100% (20/20), Fertig.


2. Wechseln in das Ina-DB Verzeichnis


```bash
cd virtualisierung-vagrant-ina_db
```


```bash
vagrant status
```

    [0mCurrent machine states:
    
    default                   not created (virtualbox)
    
    The environment has not yet been created. Run `vagrant up` to
    create the environment. If a machine is not created, only the
    default provider will be shown. So if a provider is not listed,
    then the machine is not created for that environment.[0m


4. Starten der Vagrantmaschine


```bash
vagrant up
```

    [0mBringing machine 'default' up with 'virtualbox' provider...[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Importing base box 'rdf/ubuntu2004-de-devops'...[0m
    [K[0m[1m==> default: Matching MAC address for NAT networking...[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Checking if box 'rdf/ubuntu2004-de-devops' version '0.5.2' is up to date...[0m
    [1;33m==> default: A newer version of the box 'rdf/ubuntu2004-de-devops' for provider 'virtualbox' is
    ==> default: available! You currently have version '0.5.2'. The latest is version
    ==> default: '0.5.4'. Run `vagrant box update` to update.[0m
    [1m==> default: Setting the name of the VM: virtualisierung-vagrant-ina_db_default_1626339669804_57936[0m
    [1m==> default: Fixed port collision for 22 => 2222. Now on port 2200.[0m
    [1m==> default: Clearing any previously set network interfaces...[0m
    [1m==> default: Preparing network interfaces based on configuration...[0m
    [0m    default: Adapter 1: nat[0m
    [0m    default: Adapter 2: hostonly[0m
    [1m==> default: Forwarding ports...[0m
    [0m    default: 22 (guest) => 2200 (host) (adapter 1)[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Running 'pre-boot' VM customizations...[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Booting VM...[0m
    [1m==> default: Waiting for machine to boot. This may take a few minutes...[0m
    [0m    default: SSH address: 127.0.0.1:2200[0m
    [0m    default: SSH username: vagrant[0m
    [0m    default: SSH auth method: private key[0m
    [0m    default: 
        default: Vagrant insecure key detected. Vagrant will automatically replace
        default: this with a newly generated keypair for better security.[0m
    [0m    default: 
        default: Inserting generated public key within guest...[0m
    [0m    default: Removing insecure key from the guest if it's present...[0m
    [0m    default: Key inserted! Disconnecting and reconnecting using new SSH key...[0m
    [1m==> default: Machine booted and ready![0m
    [1m==> default: Configuring proxy for Apt...[0m
    [1m==> default: Skipping configuration of docker_proxy[0m
    [1m==> default: Configuring proxy environment variables...[0m
    [1m==> default: Skipping configuration of git_proxy[0m
    [1m==> default: Configuring proxy for npm...[0m
    [1m==> default: Skipping configuration of yum_proxy[0m
    [0;33m[default] GuestAdditions versions on your host (6.1.22) and guest (6.1.18) do not match.[0m
    [0mPaketlisten werden gelesen...[0m[0m
    [0m[0mAbhängigkeitsbaum wird aufgebaut....[0m[0m
    Statusinformationen werden eingelesen....[0m[0m
    [0m[0mlinux-headers-5.4.0-71-generic ist schon die neueste Version (5.4.0-71.79).
    linux-headers-5.4.0-71-generic wurde als manuell installiert festgelegt.
    Vorgeschlagene Pakete:
      debtags menu
    [0m[0mDie folgenden NEUEN Pakete werden installiert:
    [0m[0m  dctrl-tools dkms
    [0m[0m0 aktualisiert, 2 neu installiert, 0 zu entfernen und 0 nicht aktualisiert.
    Es müssen 128 kB an Archiven heruntergeladen werden.
    Nach dieser Operation werden 599 kB Plattenplatz zusätzlich benutzt.
    Holen:1 http://us.archive.ubuntu.com/ubuntu focal/main amd64 dctrl-tools amd64 2.24-3 [61,5 kB]
    [0m[0mHolen:2 http://us.archive.ubuntu.com/ubuntu focal-updates/main amd64 dkms all 2.8.1-5ubuntu2 [66,8 kB]
    [0m[0mEs wurden 128 kB in 0 s geholt (3.747 kB/s).
    [0m[0mVormals nicht ausgewähltes Paket dctrl-tools wird gewählt.
    (Lese Datenbank ... 230690 Dateien und Verzeichnisse sind derzeit installiert.)
    [0m[0mVorbereitung zum Entpacken von .../dctrl-tools_2.24-3_amd64.deb ...
    [0m[0mEntpacken von dctrl-tools (2.24-3) ...
    [0m[0mVormals nicht ausgewähltes Paket dkms wird gewählt.
    [0m[0mVorbereitung zum Entpacken von .../dkms_2.8.1-5ubuntu2_all.deb ...
    [0m[0mEntpacken von dkms (2.8.1-5ubuntu2) ...
    [0m[0mdctrl-tools (2.24-3) wird eingerichtet ...
    [0m[0mdkms (2.8.1-5ubuntu2) wird eingerichtet ...
    [0m[0mTrigger für man-db (2.9.1-1) werden verarbeitet ...
    [0m[0mCopy iso file /usr/share/virtualbox/VBoxGuestAdditions.iso into the box /tmp/VBoxGuestAdditions.iso[0m
    [0mMounting Virtualbox Guest Additions ISO to: /mnt[0m
    [0mmount: [0m[0m/mnt: WARNUNG: das Gerät ist schreibgeschützt und wird daher im Nur-Lese-Modus eingehängt.[0m[0m
    [0m[0;33mInstalling Virtualbox Guest Additions 6.1.22 - guest version is 6.1.18[0m
    [0mVerifying archive integrity...[0m[0m All good.
    [0m[0mUncompressing VirtualBox 6.1.22 Guest Additions for Linux[0m[0m..[0m[0m.[0m[0m.[0m[0m.[0m[0m...[0m[0m
    [0m[0mVirtualBox Guest Additions installer
    [0m[0mRemoving installed version 6.1.18 of VirtualBox Guest Additions...
    [0m[0mupdate-initramfs: Generating /boot/initrd.img-5.4.0-26-generic
    [0m[0mupdate-initramfs: Generating /boot/initrd.img-5.4.0-71-generic
    [0m[0mCopying additional installer modules ...
    [0m[0mInstalling additional modules ...
    [0m[0mVirtualBox Guest Additions: Starting.
    [0m[0mVirtualBox Guest Additions: Building the VirtualBox Guest Additions kernel 
    modules.  This may take a while.
    [0m[0mVirtualBox Guest Additions: To build modules for other installed kernels, run
    [0m[0mVirtualBox Guest Additions:   /sbin/rcvboxadd quicksetup <version>
    [0m[0mVirtualBox Guest Additions: or
    [0m[0mVirtualBox Guest Additions:   /sbin/rcvboxadd quicksetup all
    [0m[0mVirtualBox Guest Additions: Building the modules for kernel 5.4.0-71-generic.
    [0m[0mupdate-initramfs: Generating /boot/initrd.img-5.4.0-71-generic
    [0m[0mVirtualBox Guest Additions: Running kernel modules will not be replaced until 
    the system is restarted
    [0m[0;33mAn error occurred during installation of VirtualBox Guest Additions 6.1.22. Some functionality may not work as intended.
    In most cases it is OK that the "Window System drivers" installation failed.[0m
    [0mUnmounting Virtualbox Guest Additions ISO from: /mnt[0m
    [0;33mGot different reports about installed GuestAdditions version:
    Virtualbox on your host claims:   6.1.18
    VBoxService inside the vm claims: 6.1.22
    Going on, assuming VBoxService is correct...[0m
    [0;33mGot different reports about installed GuestAdditions version:
    Virtualbox on your host claims:   6.1.18
    VBoxService inside the vm claims: 6.1.22
    Going on, assuming VBoxService is correct...[0m
    [0;33mGot different reports about installed GuestAdditions version:
    Virtualbox on your host claims:   6.1.18
    VBoxService inside the vm claims: 6.1.22
    Going on, assuming VBoxService is correct...[0m
    [0;33mRestarting VM to apply changes...[0m
    [1m==> default: Attempting graceful shutdown of VM...[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Booting VM...[0m
    [1m==> default: Waiting for machine to boot. This may take a few minutes...[0m
    [1m==> default: Machine booted and ready![0m
    [1m==> default: Checking for guest additions in VM...[0m
    [1m==> default: Using /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db for persistent storage.[0m
    [1m==> default: Setting hostname...[0m
    [1m==> default: Configuring and enabling network interfaces...[0m
    [1m==> default: Mounting shared folders...[0m
    [0m    default: /vagrant => /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db[0m
    [0m    default: /home/vagrant/www => /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db/www[0m
    [0m    default: /home/vagrant/notebooks => /home/michl/Dokumente/Schule/1_Semester/virtualisierung-vagrant-INA_DB/virtualisierung-vagrant-ina_db/notebooks[0m
    [1m==> default: Running provisioner: shell...[0m
    [0m    default: Running: inline script[0m
    [0;32m    default: Holen:1 https://deb.nodesource.com/node_12.x focal InRelease [4.583 B][0m
    [0;32m    default: Holen:2 http://security.ubuntu.com/ubuntu focal-security InRelease [114 kB][0m
    [0;32m    default: OK:3 http://us.archive.ubuntu.com/ubuntu focal InRelease[0m
    [0;32m    default: Holen:4 http://us.archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB][0m
    [0;32m    default: Holen:5 https://deb.nodesource.com/node_12.x focal/main amd64 Packages [767 B][0m
    [0;32m    default: Holen:6 https://download.docker.com/linux/ubuntu focal InRelease [52,1 kB][0m
    [0;32m    default: Holen:7 http://us.archive.ubuntu.com/ubuntu focal-backports InRelease [101 kB][0m
    [0;32m    default: Holen:8 http://us.archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages [1.086 kB][0m
    [0;32m    default: Holen:9 http://us.archive.ubuntu.com/ubuntu focal-updates/main i386 Packages [505 kB][0m
    [0;32m    default: Holen:10 https://download.docker.com/linux/ubuntu focal/stable amd64 Packages [9.960 B][0m
    [0;32m    default: Holen:11 http://us.archive.ubuntu.com/ubuntu focal-updates/main Translation-en [239 kB][0m
    [0;32m    default: Holen:12 http://us.archive.ubuntu.com/ubuntu focal-updates/main amd64 c-n-f Metadata [13,8 kB][0m
    [0;32m    default: Holen:13 http://us.archive.ubuntu.com/ubuntu focal-updates/restricted amd64 Packages [318 kB][0m
    [0;32m    default: Holen:14 http://us.archive.ubuntu.com/ubuntu focal-updates/restricted i386 Packages [17,7 kB][0m
    [0;32m    default: Holen:15 http://us.archive.ubuntu.com/ubuntu focal-updates/restricted Translation-en [46,1 kB][0m
    [0;32m    default: Holen:16 http://us.archive.ubuntu.com/ubuntu focal-updates/restricted amd64 c-n-f Metadata [456 B][0m
    [0;32m    default: Holen:17 http://us.archive.ubuntu.com/ubuntu focal-updates/universe i386 Packages [625 kB][0m
    [0;32m    default: Holen:18 http://us.archive.ubuntu.com/ubuntu focal-updates/universe amd64 Packages [841 kB][0m
    [0;32m    default: Holen:19 http://us.archive.ubuntu.com/ubuntu focal-updates/universe Translation-en [176 kB][0m
    [0;32m    default: Holen:20 http://us.archive.ubuntu.com/ubuntu focal-updates/universe amd64 c-n-f Metadata [18,3 kB][0m
    [0;32m    default: Holen:21 http://us.archive.ubuntu.com/ubuntu focal-updates/multiverse amd64 Packages [23,6 kB][0m
    [0;32m    default: Holen:22 http://us.archive.ubuntu.com/ubuntu focal-updates/multiverse i386 Packages [6.376 B][0m
    [0;32m    default: Holen:23 http://us.archive.ubuntu.com/ubuntu focal-updates/multiverse Translation-en [6.472 B][0m
    [0;32m    default: Holen:24 http://us.archive.ubuntu.com/ubuntu focal-updates/multiverse amd64 c-n-f Metadata [648 B][0m
    [0;32m    default: Holen:25 http://us.archive.ubuntu.com/ubuntu focal-backports/main amd64 Packages [2.568 B][0m
    [0;32m    default: Holen:26 http://us.archive.ubuntu.com/ubuntu focal-backports/main i386 Packages [2.568 B][0m
    [0;32m    default: Holen:27 http://us.archive.ubuntu.com/ubuntu focal-backports/main Translation-en [1.120 B][0m
    [0;32m    default: Holen:28 http://us.archive.ubuntu.com/ubuntu focal-backports/main amd64 c-n-f Metadata [400 B][0m
    [0;32m    default: Holen:29 http://us.archive.ubuntu.com/ubuntu focal-backports/universe i386 Packages [4.724 B][0m
    [0;32m    default: Holen:30 http://us.archive.ubuntu.com/ubuntu focal-backports/universe amd64 Packages [5.792 B][0m
    [0;32m    default: Holen:31 http://us.archive.ubuntu.com/ubuntu focal-backports/universe Translation-en [2.060 B][0m
    [0;32m    default: Holen:32 http://us.archive.ubuntu.com/ubuntu focal-backports/universe amd64 c-n-f Metadata [288 B][0m
    [0;32m    default: Holen:33 http://security.ubuntu.com/ubuntu focal-security/main i386 Packages [258 kB][0m
    [0;32m    default: Holen:34 http://security.ubuntu.com/ubuntu focal-security/main amd64 Packages [745 kB][0m
    [0;32m    default: Holen:35 http://security.ubuntu.com/ubuntu focal-security/main Translation-en [148 kB][0m
    [0;32m    default: Holen:36 http://security.ubuntu.com/ubuntu focal-security/main amd64 c-n-f Metadata [8.036 B][0m
    [0;32m    default: Holen:37 http://security.ubuntu.com/ubuntu focal-security/restricted amd64 Packages [282 kB][0m
    [0;32m    default: Holen:38 http://security.ubuntu.com/ubuntu focal-security/restricted i386 Packages [16,4 kB][0m
    [0;32m    default: Holen:39 http://security.ubuntu.com/ubuntu focal-security/restricted Translation-en [40,9 kB][0m
    [0;32m    default: Holen:40 http://security.ubuntu.com/ubuntu focal-security/restricted amd64 c-n-f Metadata [456 B][0m
    [0;32m    default: Holen:41 http://security.ubuntu.com/ubuntu focal-security/universe amd64 Packages [629 kB][0m
    [0;32m    default: Holen:42 http://security.ubuntu.com/ubuntu focal-security/universe i386 Packages [503 kB][0m
    [0;32m    default: Holen:43 http://security.ubuntu.com/ubuntu focal-security/universe Translation-en [96,2 kB][0m
    [0;32m    default: Holen:44 http://security.ubuntu.com/ubuntu focal-security/universe amd64 c-n-f Metadata [11,6 kB][0m
    [0;32m    default: Holen:45 http://security.ubuntu.com/ubuntu focal-security/multiverse amd64 Packages [19,9 kB][0m
    [0;32m    default: Holen:46 http://security.ubuntu.com/ubuntu focal-security/multiverse i386 Packages [5.384 B][0m
    [0;32m    default: Holen:47 http://security.ubuntu.com/ubuntu focal-security/multiverse Translation-en [4.316 B][0m
    [0;32m    default: Holen:48 http://security.ubuntu.com/ubuntu focal-security/multiverse amd64 c-n-f Metadata [528 B][0m
    [0;32m    default: Es wurden 7.106 kB in 3 s geholt (2.771 kB/s).[0m
    [0;32m    default: Paketlisten werden gelesen...[0m
    [0;32m    default: Hallo Welt![0m
    [1m==> default: Skipping configuration of docker_proxy[0m
    [1m==> default: Skipping configuration of git_proxy[0m
    [1m==> default: Configuring proxy for npm...[0m


### Jupyter Notebook

Findet man unter:

* http://192.168.33.10:8888

PW: `jns`

### PhpMyadmin

http://192.168.33.10/phpmyadmin

Benutzer: admin

Passwort: passw0rd


```bash
vagrant list-commands
```

    [0mBelow is a listing of all available Vagrant commands and a brief
    description of what they do.
    
    autocomplete    manages autocomplete installation on host
    box             manages boxes: installation, removal, etc.
    cap             checks and executes capability
    cloud           manages everything related to Vagrant Cloud
    destroy         stops and deletes all traces of the vagrant machine
    docker-exec     attach to an already-running docker container
    docker-logs     outputs the logs from the Docker container
    docker-run      run a one-off command in the context of a container
    global-status   outputs status Vagrant environments for this user
    halt            stops the vagrant machine
    help            shows the help for a subcommand
    init            initializes a new Vagrant environment by creating a Vagrantfile
    list-commands   outputs all available Vagrant subcommands, even non-primary ones
    login           
    package         packages a running vagrant environment into a box
    plugin          manages plugins: install, uninstall, update, etc.
    port            displays information about guest port mappings
    powershell      connects to machine via powershell remoting
    provider        show provider for this environment
    provision       provisions the vagrant machine
    push            deploys code in this environment to a configured destination
    rdp             connects to machine via RDP
    reload          restarts vagrant machine, loads new Vagrantfile configuration
    resume          resume a suspended vagrant machine
    rsync           syncs rsync synced folders to remote machine
    rsync-auto      syncs rsync synced folders automatically when files change
    snapshot        manages snapshots: saving, restoring, etc.
    ssh             connects to machine via SSH
    ssh-config      outputs OpenSSH valid configuration to connect to the machine
    status          outputs status of the vagrant machine
    suspend         suspends the machine
    up              starts and provisions the vagrant environment
    upload          upload to machine via communicator
    user            manage Nugrant user defined parameters (config.user)
    validate        validates the Vagrantfile
    vbguest         plugin: vagrant-vbguest: install VirtualBox Guest Additions to the machine
    version         prints current and latest Vagrant version
    winrm           executes commands on a machine via WinRM
    winrm-config    outputs WinRM configuration to connect to the machine[0m


5. Stoppen der Maschine


```bash
vagrant halt
```


```bash
vagrant status
```
